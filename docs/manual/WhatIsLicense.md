# ライセンスについて
<!--  
//wikiのライセンスのページがLTOLのページになってるので  
//適当にでっち上げた  
-->  

元となったページ
[eratohoまとめ V3 ライセンスについて](https://seesaawiki.jp/eratoho/d/%a5%e9%a5%a4%a5%bb%a5%f3%a5%b9%a4%cb%a4%c4%a4%a4%a4%c6)

---

## ライセンスについてどうしたらいいのか３行で  

**トラブル回避のために**  
**口上やバリアント・改造パッチには**  
**改変や再配布の許可・不許可を明記することをオススメ**  

---  

## ライセンスって何？  
>ライセンス（米: license、英: licence）は、それが存在しなければ違法となる行為をすることを許可すること、あるいはその許可を称する書面のことをいう。  
>[wikipediaより](http://ja.wikipedia.org/wiki/%E3%83%A9%E3%82%A4%E3%82%BB%E3%83%B3%E3%82%B9)  

よくreadmeの後ろの方に書いてあるアレです  

---

## なぜライセンスが必要？  
eraの改造が行われ始めたころは改変は暗黙の了解の下に行われていましたが  
多くの人が制作にかかわるようになった今、トラブルを防ぐために改変・再配布などの許可・不許可を明記することが推奨されています。  
**自分の作ったものは<span style="color:red">好きにしていいという場合でも</span> 、何も書かなかった場合「全て不許可」扱いされてしまうので、<span style="color:red">一言添えることをお勧め</span>します。**  

**<span style="color:red">実際に口上を商業に無断使用された前例があります</span>**  

**改変などを許可する場合口上の使用可能範囲（era以外では使用禁止、商業利用禁止など）も記しておくことを強くお勧めします**  
詳しい経緯については掲示板の[東方鉄鎖録の問題について](http://jbbs.shitaraba.net/bbs/read.cgi/otaku/16783/1448035831/)を御覧ください。  

---

## どんなことを書けばいい？  

- **改変**（構文の修正や口上の加筆など）の**許可・不許可**  
- **再配布**（まとめへの収録や再アップロード）の**許可・不許可**  

最低この二点について書いてあれば大丈夫です  
細かいことを気にしないのであれば、**無理に下のチェックリストやライセンステンプレートを使う必要はありません。**  

なお、口上以外の機能拡張もしくは改造パッチに関しては以下のことも記述しておくといいでしょう。  

- **取込**（バリアント本体への機能取り込み）の**許可・不許可**  
- **移植**（目的バリアントと違うバリアントへの移植）の**許可・不許可**  

**特に理由がないのであれば改変・再配布を許可することをお勧めします。**  

<!--
//**ライセンス文例  
//-「改変・再配布等はご自由にどうぞ」  
//ライセンスとか面倒だ！という人のためのオススメライセンス文。コピペしてどうぞ  
//--「（上に追記）ただし改変の際は改変であることを明記すること」  
//よく使われるパターン  
//-「バグ修正・口上まとめ以外の改変再配布を禁ず」  
//俺のジャスティスを汚されたくない人のためのオススメ。  
-->

---

## ライセンスを細かく書きたい人へ  

### 改変・再配布の許可が必要になるケース  

- アップローダのトラブルなどによるファイルの消滅の際の再アップロード  
トラブルの際に作者不在で復旧不能になるのを防ぐため  
- 口上まとめへの収録  
- バリアント・改造パッチ  
改変ができないと不便  

### 不許可にするケース  

- 改変  
自分のジャスティスを汚されたくない  
- 二次配布  
まだ未完成なので  
特殊な口上なので（ネタ口上など）  
全て自分が把握してないと嫌だ  

### 口上のライセンスを書く際のチェックリスト  

１．口上について  
　　次の各項目を明記する  

- 口上名（一目で特徴が分かる名前だとベター）  
- 口上作者名（XXスレ目>>123 等でもOKなので、何か他人と被らないものを）（加筆等の場合は元作者の分も）  
- 連絡先（メールアドレスもしくはブログ、いずれもないor公開したくない場合は、IRCで反応できるchおよび時間帯もしくは雑談スレか本スレを見ている頻度など）
  複数の手段が取れるなら併記しておくとさらによい。  
- 更新日orバージョン  

以上の各項目はライセンスとは直接関係無いが、アップデートの際にどの口上のバージョンアップなのかわかりにくくなるなど不都合が起きるので書くことをお勧めします。  

２．修正・改変  
　　次の内どこを弄っていいかはっきりさせる  

- 明らかな誤字の訂正  
- 動作に支障のあるERB構文の訂正  
- ERB内のコメントと実際の動作の食い違いをコメントにあわせた動作に修正  
- バリアント本体のバージョンアップに対応させるための修正  
- 他バリアントへの口上の一部・全部移植  
- すでにある口上を改変しない範囲での加筆  
- 口上内の一部機能もしくは関数のみの引用や移植  
- すべて自由にしていい  

３．二次配布  

- 再アップロード  
- 口上まとめへの収録  

<!--
//４．その他  
//-ライセンスの継承  
// 改変後のライセンスについて  
//-時限ライセンス  
// 一定期間たったらフリーに  
// LTOL  
//-作者の許可を必要とする項目について  
// 連絡が取れなくなった場合、実質不許可と同等になる  
-->

### ライセンステンプレート  
有志によって作られたライセンスのテンプレートです。  
このライセンスを使わなければならないという決まりではありません。  
[LTOLライセンスについて](LTOL-license.md)  
<!---[[eratohoYM用 口上テンプレート内ライセンステンプレ>テンプレ・ツール#YM_KOJO_L]]-->  
