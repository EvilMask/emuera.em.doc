# バリアント製作/タイトル準備編  

元となったページ  
[eraシリーズを語るスレ まとめWiki V3 タイトル準備編](https://seesaawiki.jp/eraseries/d/%a5%bf%a5%a4%a5%c8%a5%eb%bd%e0%c8%f7%ca%d4)  

---

- [チュートリアル](erawiki-tutorial.md)
- タイトル準備編
- [タイトル実践編](erawiki-title2.md)
- [ERB製作実践編](erawiki-ERBmanual.md)

---  

## タイトル画面を作ろう準備編  
バニラのシステムを把握はできていないが、はじめの環境は整った。  

早くゲームを作りたい。  
その心意気を示すタイトル画面を作ろう。  

既存のバリアント作品を思い浮かべると、  
統一規格があるかのように見慣れたシンプルな黒い画面だったり、  
アスキーアートだったり、画像を利用していたり、色々なタイトル画面がある。  

いきなり見出しと矛盾したことを言うようだが、  
実はeraのタイトル画面は作る必要がない。  

なので、この項目はしばらく見出し詐欺になる。  
が、最終的には自力で作る場合でも役立つはずなので読んでみて欲しい。  

---  

### GameBase.csv  

CSVフォルダ、`GameBase.csv`を開いてみよう。  

```  
コード,326136  
バージョン,110  
タイトル,erakanon(minimum)  
作者,佐藤敏（サークル獏）  
製作年,2005-2006  
追加情報,（※これは調教SLG作成ツールeramakerのサンプルゲームです。）  
```  

と書かれている。  

---  

### タイトルを変える  

```
タイトル,erakanon(minimum)  
```

を  

```
タイトル,あなたの考えるタイトル  
```

に変え、保存して、Emuera.exeを起動してみよう。  

ウィンドウの左上に表示される名前が  

>あなたの考えるタイトル 0.11  

画面中央が  

>あなたの考えるタイトル  

に変わったと思う。  

もうお気づきかもしれないが、`Gamebase.csv`を変更するだけで、  
表示される情報が変わり、タイトル画面ができてしまうのだ。  

『違う！　俺はもっとカッコいいタイトルをイメージできているんだ！　自分で作りたいんだよ！！』  
と考える人もいると思う。  
しかし、タイトル画面を自作したい場合でも`Gamebase.csv`に入力した情報は利用してみよう。  

気が変わってタイトルを変えたくなったとき、  
あるいは更新を進めてゲームバージョンを変更したくなったとき。  
変更しなければならない場所は、少なければ少ないほどいい。  

自作する場合でも『Gamebase.csvだけ変更したら、自動的にタイトル画面に表示される情報が変わる仕組み』  
に作れば、後々の変更点が一か所にまとまって楽になる。  

それだけなら、  
『Gamebase.csvを使わないで、タイトル表示用のファイルだけ管理すれば、変更点が一か所で済むのは同じじゃね？』  
と思うかもしれない。  

が、`Gamebase.csv`変数は代入不可（WINDOW_TITLEは代入可能）という都合や  
バージョンによるセーブデータの読み込み可否を管理をしてくれるなど、  
`Gamebase.csv`を指定すると便利なことがある。  
他の人が見たとき、どこに情報があるかわかりやすいという利点もある。  
`Gamebase.csv`を確認をしてバージョンを確認した上でエラー報告してくれる人もいる。  

なので実際に使用するかどうかはともあれ、どういった使い方ができるのかは知っておくと便利。  

---  

### ウィンドウタイトルを変える  

```
タイトル,あなたの考えるタイトル  
```

の次行くらいに、  

```
ウィンドウタイトル,あなたの考えるウィンドウタイトル  
```

という行を追加してみよう。  

画面中央は  

>『あなたの考えるタイトル』  

のまま、  
ウィンドウの左上に表示される名前が  

>『あなたの考えるタイトル 0.11』  

から  

>『あなたの考えるウィンドウタイトル』  

に変更されたと思う。  

これについてeramakerの説明書には記載がない。  
Emueraに追加された機能だからだ。  

- [EmueraWiki→変数→csv関連→WINDOW_TITLE](https://evilmask.gitlab.io/emuera.em.doc/Emuera/variables.html#window_title)  

```
初期値はgamebase.csvの"ウィンドウタイトル"に設定された値です。  
"ウィンドウタイトル"が設定されていなければ  
"タイトル"と"バージョン"から生成します。  
"タイトル"も設定されていない場合、"Emuera"になります。  
```

と書かれている。  

タイトルにバージョンは表示したくないなど、  
表示を変えたい理由があるときはウィンドウタイトルを指定するとよさそうだ。  

---  

### 作者を変える  

```
作者,佐藤敏（サークル獏）  
```

を  

```
作者,あなたのハンドルネーム  
```

にしてみよう。  

このバリアントの作者はあなただ。  
作者名を変えることを想定して`Gamebase.csv`に項目が用意されているようなので、  
ここの作者の項目は変えてしまって問題ない。  

eraバニラやEmueraの製作者様方の著作権については、別途Readmeを同梱して読んでいただこう。  
（Emueraは同梱を想定したreadmeがある。  
　eramakerにライセンス表記が独立しているreadmeはなさそうだ。  
　タイトルの追記紹介に残す、自前のreadme側にサイト紹介を貼る、  
　ライセンス表記のある説明書同梱などで、何らかの誘導という形になる）  

先人を敬い著作権を尊重することは大切だ。  
しかし遠慮して著作者をそのままにすると、別の問題が起きる。  
ツール製作者様やベースバリアント製作者様が自作品の製作に関与していると誤解されたり、  
改変によって起きたエラーの問い合わせが先方にいく、というご迷惑をかけてしまう。  

なので問い合わせ先は明示しよう。  
その上で、問い合わせには答えられないと免責を書くこともできる。  

---  

### ライセンスについて  
著作権について少し触れた。  
ライセンスについて書いてくれている方がいるので、学んでトラブルを回避しよう。  

- [製作初心者講座→製作に役立つリンク→ライセンスについて](https://evilmask.gitlab.io/emuera.em.doc/manual/WhatIsLicense.html)  

---  

### コードってなに？  
他の項目も弄ってみよう。  

一番よくわからないのは  

>コード,326136  

の行だと思う。  

意味はわからなくてもいいからさっさと進めたい人のために結論を先に書くと、  
とりあえず100000～100000000くらいのかぶりにくそうな数字を置いておけばいい。  

- [漠々ト、獏 eramaker CSVファイル書式（暫定版）](https://cbaku2.sakura.ne.jp/b/erakanon/eramacsv.html)  

を見ると、  

>コード,(数値)  
>ゲームコードを(数値)に設定します。  
>これは、誤って別のゲームのセーブデータをロードしてしまうことを防ぐために使います。  
>(数値)は適当な値でかまいません。  

と書かれている。  

他のゲームとかぶらない番号ならなんでもいい、IDのようなものらしい。  

それならできるだけ多くの桁を指定し、極力被らない数字にしたい派もいる。  
桁の限界はいくつだろうか。  

- [EmueraWiki→eramakerとの相違点→gamebase.csv『コード』の読み方](https://evilmask.gitlab.io/emuera.em.doc/Emuera/differences_of_Emuera_and_eramaker.html#gamebasecsv)  

ここに書かれていることを読むと、  
eramakerの場合は、桁数が限度を超えると部分的にとって自動指定する。  
<!--
桁数まで本当になんでもよかったらしい。  
しかし実際に指定した数値通り扱われるわけではないというわかりにくい面もある。  
-->

Emueraの場合は、桁数が限界を超えると0として扱うようだ。  
0の場合、ゲームコードに関わらず読み込みにいくよと書かれている。  
『`-9223372036854775808～9223372036854775807`』の範囲(64bit)。  

誤作動を防止する目的なので、同じ数字の羅列・切りのいい番号のような、かぶりやすい数字は避けよう。  

<!-->
変数について詳細が書かれているeratohoまとめV3へのリンク一覧のあるページ。  
[[応用編]]  
-->

---  

### バージョンってなに？  
常に上書きしていると失敗したとき元に戻せない。  
一区切りついたところで保存して、コピーをもとに新しい要素を追加したほうがいい。  
そうすれば失敗したとき、保存してあるデータに戻しやすい。  

そのバックアップがぐちゃぐちゃにならないように、  
製作の進行度に応じて付けておく番号がバージョンだ。  

更新するたびに数字をあげることで、どこまで更新されたかわかりやすくする。  

- [eramaker CSVファイル書式（暫定版）](https://cbaku2.sakura.ne.jp/b/erakanon/eramacsv.html)  

を見ると  

>画面上は、(数値)を1000で割ったものが表示されます（100なら0.10）  

と説明されている。  

現在、バージョンは  

```
バージョン,110  
```

になっている。  

これを  

```
バージョン,100  
```

にしてみよう。  

タイトル画面に表示される数字が、0.10になる。  

```
バージョン,112  
```

にしてみよう。  

タイトル画面に表示される数字が、0.112になる。  

小数点以下2桁まで表示されるのがデフォルトで、  
小数点以下3桁目は、指定がなければ省略される。  

とりあえずそれを頭の片隅においた上で、  
実際どのように管理するかは人それぞれで、  
自分が管理しやすいように指定しよう。  

例をあげると  

- １桁目がメジャーバージョン  
（全体に影響するような巨大な更新であったり、セーブデータの互換性を失うときに）  

- 小数点以下１～２桁目がマイナーバージョン  
（機能などの細々とした追加を行うときに）  

- 小数点以下３桁目が不具合修正  

といった分け方や（たぶんデフォルト設定で想定されている分け方）  

１桁目はメジャーバージョン  
小数点以下はマイナーバージョン  
といった分け方や  

１桁目がメジャーバージョン  
小数点以下１桁目がマイナーバージョン  
小数点以下２桁目がパッチ  
小数点以下３桁目が不具合修正  
といった分け方や  

自動的に割り算される表示を使わないようにして桁数を一万に増やし、  
`X.XX.XX`と表示して、互換性喪失.要素追加.不具合修正とする  
といった分け方などがある。  

なんにせよ、パッチ作者は`Gamebase.csv`を弄らないことが多いので、  
まとめてくれる人がいないと触れられることすらないという状況もありえる。  

バージョン管理システムというものもあり、活用している方が多い。  

- [eraシリーズを語るスレ　まとめWiki　V3→システム改造Q&A→その他→バージョン管理システムを使う](https://evilmask.gitlab.io/emuera.em.doc/manual/erawiki-modification-QandA.html#_17)  

---  

### 製作年ってなに？  
そのまま、作られた時期。  

```
製作年,2005-2006  
```

を見て驚くかたもいるだろう。  
歴史の厚みを感じる。  

これから作る人はとりあえず  

```
製作年,2024  
```

としておこう。(2024年現在)  

---  

### 追加情報ってなに？  

```
追加情報,（※これは調教SLG作成ツールeramakerのサンプルゲームです。）  
```

ここはそれぞれの書きたい追加情報で良い。  
ツールの紹介、元バリアントの紹介、年齢の警告、あえて表示せず別途書くなど。  

---  

### バージョン違い認める？  
最初は最小のバージョンに設定しておく。バージョンを1にしたなら1に。  

互換性を失う更新のとき、互換性がないよ、と告知するだけだと、  
気付かなかったプレイヤーが古いデータで遊び続け、不具合としてエラー報告する  
ということがある。  

互換性を失う更新をするとき、ここを設定しておくと、  
そのバージョン以前の古いセーブデータを読み込みできなくなる。  

一部の変数を入れ替えるなどの更新をした場合、通常は  
ロード直後に読み込まれる『`@EVENTLOAD`』内などに齟齬を埋める処理を書き、  
互換性を失わないようにする。  
<!--//(2021/05/12 wiki編集スレでご指摘いただいて追記。感謝）  -->

互換性を切るのは、また最初から遊んでみたくなるほど  
大規模な更新をするタイミング以外では、わりと最後の手段だ。  
遊んでくれる人が多いほど、  
『大事なセーブデータを読み込みできなくなるくらいならバージョンアップしなければよかった！』  
とショックを受ける人も多くなる。  
短いゲームならこの限りではないかもしれない。  

互換性を失って不具合が出る可能性があっても、セーブデータを使えなくしてしまうよりは、  
と、あえて認めるケースもたぶんある。  

---  

### 最初からいるキャラ、アイテムなし  
作るゲームによるので、ひとまず割愛。  
最初からいるキャラはeralightの場合に使用する。  

---  

### 入力した情報が格納されている変数  
これらの情報は以下の変数に格納されているため  
ゲーム内から呼び出して使用することができる。  

- [EmueraWiki→eramaker basic 開発者向け情報→Emueraで追加された拡張文法→定数・変数](https://evilmask.gitlab.io/emuera.em.doc/Emuera/variables.html#gamebasecsv)  

タイトルを自作する場合でも、これらの変数を利用して表示したら、  
更新時の変更点が`Gamebase.csv`だけで済む、ということになる。  

非配列、代入不可、セーブされない変数と書かれている通り、  
ERB側からこれらを書き換えることはできない。  

---

次のページ→[タイトル実践編](erawiki-title2.md)
