---
hide:
  - toc
---

# eramakerの仕様

eramakerの仕様についてまとめています
[漠々ト、獏公式サイト](http://cbaku.com/b/2010/12/eramaker/)からの引用多め

- [システム関数・フローについて](system_flow.md)  
    - ゲーム起動からシステム関数の流れについて。レガシー要素が多いものの、基本的な仕様はEmueraに受け継がれている

- [CSVの書式](CSV_format.md)  
    - CSVを制作する際の記法、ルールについて。Emueraともある程度の互換性が保たれた内容

- [ERB書式・命令について](ERB_format.md)  
    - ERBファイルの記法、基本的な命令の仕様。レガシー要素が多いため、サンプルコードの流用は非推奨

- [eramaker用変数リスト](variables.md)  
    - eramakerで使用できる変数のリスト。すべてEmueraでも使用可能
