---
hide:
  - toc
---

# CALLEVENT

| 関数名                                                             | 引数           | 戻り値 |
| :----------------------------------------------------------------- | :------------- | :----- |
| ![](../assets/images/IconEmuera.webp)[`CALLEVENT`](./CALLEVENT.md) | `functionName` | なし   |

!!! info "API"

    ```  { #language-erbapi }
	CALLEVENT eventFuction
    ```
	イベント関数をイベント関数として呼び出します。  
	引数を与えることはできません。  
	またイベント関数中やイベント関数から呼び出された関数中で使用することもできません。  

!!! hint "ヒント"

    命令のみ対応しています。
