---
hide:
  - toc
---

# BARSTR

| 関数名                                                       | 引数                | 戻り値  |
| :----------------------------------------------------------- | :------------------ | :------ |
| ![](../assets/images/IconEmuera.webp)[`BARSTR`](./BARSTR.md) | `int`, `int`, `int` | `string`|

!!! info "API"

    ```  { #language-erbapi }
	string BARSTR value, maxValue, length
    ```
	与えられた引数と同一の引数の[`BAR`](./BAR.md)命令で表示される文字列と同じ物をRESULTS:0に代入もしくは返します

!!! hint "ヒント"

    命令、式中関数両方対応しています。

### 関連項目
* [BAR](BAR.md)
