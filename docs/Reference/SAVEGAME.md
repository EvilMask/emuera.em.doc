---
hide:
  - toc
---

# SAVEGAME, LOADGAME

| 関数名                                                             | 引数   | 戻り値 |
| :----------------------------------------------------------------- | :----- | :----- |
| ![](../assets/images/Iconeramaker.webp)[`SAVEGAME`](./SAVEGAME.md) | なし   | なし   |
| ![](../assets/images/Iconeramaker.webp)[`LOADGAME`](./SAVEGAME.md) | なし   | なし   |

!!! info "API"

    ```  { #language-erbapi }
	SAVEGAME
	LOADGAME
    ```
	`SAVEGAME`はセーブ画面を、`LOADGAME`はロード画面を呼びます。いずれも`SHOP`でないと呼び出せません。

!!! hint "ヒント"

    命令のみ対応しています。
