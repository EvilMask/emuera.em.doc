---
hide:
  - toc
---

# STOPBGM

| 関数名                                                     | 引数   | 戻り値 |
| :--------------------------------------------------------- | :----- | :----- |
| ![](../assets/images/IconEE.webp)[`STOPBGM`](./STOPBGM.md) | `void` | `void` |

!!! info "API"

	``` { #language-erbapi }
	STOPBGM
	```

	`PLAYBGM`で再生中の音声を停止する

!!! hint "ヒント"

    命令としてのみ使用可能
