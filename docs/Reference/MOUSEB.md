---
hide:
  - toc
---

# MOUSEB

| 関数名                                                   | 引数 | 戻り値   |
| :------------------------------------------------------- | :--- | :------- |
| ![](../assets/images/IconEE.webp)[`MOUSEB`](./MOUSEB.md) | なし | `string` |

!!! info "API"

	``` { #language-erbapi }
	string MOUSEB
	```

	マウスオーバー中のボタン内容を取得する

!!! hint "ヒント"

    式中関数対応  
	AWAITと組み合わせて使う  
	実行時点では`INPUT`か`INPUTS`か確定していないため、文字列型で返される

### 関連項目
- [AWAIT](AWAIT.md)
