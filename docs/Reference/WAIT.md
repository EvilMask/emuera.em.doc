---
hide:
  - toc
---

# WAIT

| 関数名                                                     | 引数   | 戻り値     |
| :--------------------------------------------------------- | :----- | :--------- |
| ![](../assets/images/Iconeramaker.webp)[`WAIT`](./WAIT.md) | なし   | `void`     |

!!! info "API"

    ```  { #language-erbapi }
	WAIT
    ```
    1回のマウスクリックもしくはEnterキーの入力を待つ


!!! hint "ヒント"

    命令のみ対応しています。
