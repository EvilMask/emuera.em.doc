---
hide:
  - toc
---

# ISACTIVE

| 関数名                                                           | 引数 | 戻り値 |
| :--------------------------------------------------------------- | :--- | :----- |
| ![](../assets/images/IconEmuera.webp)[`ISACTIVE`](./ISACTIVE.md) | なし | `int`  |

!!! info "API"

    ```  { #language-erbapi }
	ISACTIVE
    ```
	Emueraのウインドウの状態を返します。  
	アクティブであれば1、アクティブでなければ0を返します。  

!!! hint "ヒント"

    命令、式中関数両方対応しています。
