---
hide:
  - toc
---

# PRINT_SPACE

| 関数名                                                                 | 引数 | 戻り値 |
| :--------------------------------------------------------------------- | :--- | :----- |
| ![](../assets/images/IconEmuera.webp)[`PRINT_SPACE`](./PRINT_SPACE.md) | `int`| なし   |

!!! info "API"

    ```  { #language-erbapi }
	PRINT_SPACE width
    ```
    フォントサイズの引数％分だけ何も表示しないスペースを作ります。  
	[`HTML_PRINT`命令の`<shape type='space'>`タグ](../Emuera/HTML_PRINT.md#shape)に相当します。  
	EM+EEにて`px`指定での表記もできるようになりました。


!!! hint "ヒント"

    命令のみ対応しています。
