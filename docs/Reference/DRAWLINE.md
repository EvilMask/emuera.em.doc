---
hide:
  - toc
---

# DRAWLINE

| 関数名                                                             | 引数 | 戻り値 |
| :----------------------------------------------------------------- | :--- | :----- |
| ![](../assets/images/Iconeramaker.webp)[`DRAWLINE`](./DRAWLINE.md) | なし | なし   |

!!! info "API"

    ```  { #language-erbapi }
	DRAWLINE
    ```
    画面の右端から左端まで`-`で線を引く


!!! hint "ヒント"

    命令のみ対応しています。


!!! example "例" 
    
    ``` { #language-erb title="MAIN.ERB" }
    @SYSTEM_TITLE 
		DRAWLINE
		WAIT
    ``` 
    ``` title="結果"
	------------------------------------------------------------------------------------------------------------------------------------------------
    ```

### 関連項目
- [CUSTOMDRAWLINE](CUSTOMDRAWLINE.md)
